<div class="min-h-screen flex flex-col sm:justify-left items-left pt-6 sm:pt-0 bg-gray-100" id="painel-login">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>




