@extends('admin.layouts.main-content')

@section('content')
	<div class="content">
		<div class="container-fluid p-0">
			<div class="mb-3">
				<h1 class="h3 d-inline align-middle">Olá, {{ Auth::user()->name }}.</h1>
				<a class="h3 d-inline align-middle" href="#">
			      Aqui você acompanha da melhor forma os seus atendimentos! 
			  	</a>
			</div>
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title mb-0">Mural de Avisos</h5>
						</div>
						<div class="card-body">
							<p class="card-text"></p>
							<a href="#" class="card-link"></a>
							<a href="#" class="card-link"></a>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title mb-0">Mensagens</h5>
						</div>
						<div class="card-body">
							<p class="card-text"></p>
							<a href="#" class="btn btn-primary"></a>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title mb-0">Agenda</h5>
						</div>
						<div class="card-body">
							<p class="card-text"></p>
							<a href="#" class="card-link"></a>
							<a href="#" class="card-link"></a>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title mb-0">Central de Ajuda</h5>
						</div>
						<div class="card-body">
							<p class="card-text"></p>
							<a href="#" class="btn btn-primary"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop