@extends('admin.layouts.main-header')
@section('main-content')

	@include('admin.layouts.navbar')
	<div class="main">
		@include('admin.layouts.navbar-colapse')
        @yield('content')
	</div>
	
@stop