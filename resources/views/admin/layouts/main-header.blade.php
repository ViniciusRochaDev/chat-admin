<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" href="img/icons/icon-48x48.png" />
		<link rel="canonical" href="https://demo-basic.adminkit.io/ui-cards.html" />
		<title>ChatAdmin | Sistema de Gerenciamento de Atendimentos</title>
		<link href="{{ asset('css/admin-panel.css') }}" rel="stylesheet">
		<link href="{{ asset('css/reset-admin-panel.css') }}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
	</head>

	<body>
		<div class="wrapper">
			@yield('main-content')
		</div>
		<script src="{{ asset('js/admin-panel.js') }}"></script>
	</body>

</html>