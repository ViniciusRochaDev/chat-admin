
	<nav id="sidebar" class="sidebar js-sidebar">
		<div class="sidebar-content js-simplebar">
			<a class="sidebar-brand" href="index.html">
	  			<span class="align-middle">ChatAdmin</span>
			</a>
			<ul class="sidebar-nav">
				<li class="sidebar-header">
					Menu
				</li>
				<li class="sidebar-item">
					<a class="sidebar-link" href="{{ url('dashboard') }}">
							<i class="align-middle" data-feather="home"></i> 
							<span class="align-middle">Dashboard</span>
					</a>
				</li>
				<li class="sidebar-item">
					<a class="sidebar-link" href="conversas_ativas.html">
							<i class="align-middle" data-feather="message-circle"></i> 
							<span class="align-middle">Chat</span>
						</a>
				</li>
				<li class="sidebar-item">
					<a class="sidebar-link" href="charts-chartjs.html">
						<i class="align-middle" data-feather="check-square"></i> 
						<span class="align-middle">Ticket</span>
					</a>
				</li>
				<li class="sidebar-item">
					<a class="sidebar-link" href="charts-chartjs.html">
					  	<i class="align-middle" data-feather="bar-chart-2"></i> 
					  	<span class="align-middle">Indicadores</span>
					</a>
				</li>	
				<li class="sidebar-item">
					<a class="sidebar-link" href="charts-chartjs.html">
	  					<i class="align-middle" data-feather="file-text"></i> 
	  					<span class="align-middle">Relatórios</span>
					</a>
				</li>	
				<li class="sidebar-item">
					<a class="sidebar-link" href="pages-profile.html">
		  				<i class="align-middle" data-feather="user"></i> 
		  				<span class="align-middle">Usuário</span>
					</a>
	   			</li> 
				<li class="sidebar-item">
					<a class="sidebar-link" href="Agenda.html">
						<i class="align-middle" data-feather="calendar"></i> 
						<span class="align-middle">Agenda</span>
					</a>
					</li> 
			</ul>
		</div>
	</nav>